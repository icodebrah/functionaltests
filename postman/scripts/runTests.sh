#!/bin/bash
#   Newman command line statement generator
#   

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
RESULTS=PASSING
REPORT="$DIR/../newman/report.xml"

declare -A FUNCTIONALTESTSUITE
FUNCTIONALTESTSUITE[LabResults]="LabResults.postman_collection.json"
FUNCTIONALTESTSUITE[Labs]="Labs.postman_collection.json"
FUNCTIONALTESTSUITE[Notifications]="Notifications.postman_collection.json"
FUNCTIONALTESTSUITE[Orders]="Orders.postman_collection.json"
FUNCTIONALTESTSUITE[OrderStatus]="OrderStatus.postman_collection.json"
FUNCTIONALTESTSUITE[PartnerEndpoint]="PartnerEndpoint.postman_collection.json"
FUNCTIONALTESTSUITE[PendingOrders]="PendingOrders.postman_collection.json"
FUNCTIONALTESTSUITE[Results]="Results.postman_collection.json"
FUNCTIONALTESTSUITE[RoundTripOrderResults]="Round Trip (Order-Results).postman_collection.json"
FUNCTIONALTESTSUITE[Routes]="Routes.postman_collection.json"
FUNCTIONALTESTSUITE[SHIM_LabPartnerLegacyApi]="SHIM_LabPartnerLegacyApi.postman_collection.json"

while getopts ":e:c:h" opt; do
    case $opt in
    e | -environment)
        E=$OPTARG
        echo "Environment set to: $OPTARG" >&2
      ;;
    c)
        C=$OPTARG
        echo "Running test collection for: $OPTARG" >&2
        ;;
    h)
        display_help
        exit 0
        ;;
    \?)
        echo "Invalid option: -$OPTARG" >&2
        display_help
        exit 0
        ;;
    :)
        echo "Option -$OPTARG requires an argument." >&2
        exit 0
        ;;
    esac
done

#########################
# The command line help #
#########################
display_help() {
    echo "Usage: $0 -e [environment] -c [collection] [options...]" >&2
    echo
    echo "   -e         Set the given environment"
    echo "   -c         Set collection to run. (i.e. jwt, labs, orders,...) "
    exit 1
}

setEnvironment(){
    if [ "${1,,}" == "dev" ] || [ "${1,,}" = "sn" ] ; then
        ENVIRONMENT="Tin(Sn)-Dev"
    
    elif [ "${1,,}" == "qa" ] || [ "${1,,}" == "ni" ]; then
        ENVIRONMENT="Nickle(Ni)-QA"

    elif [ "${1,,}" == "Stage" ] || [ "${1,,}" == "pd" ]; then
        ENVIRONMENT="Palladium(Pd)-Stage"
    else
        echo "Environment (-e) needs to be set"
    fi
}

setCollection(){

    if [ "${1,,}" == "functional" ] ; then
        COLLECTION="functional"

    elif [ "${1,,}" == "jwt" ] ; then
        COLLECTION="jwt_tokenGeneration.postman_collection.json"
    
    elif [ "${1,,}" == "labresults" ] ; then
        COLLECTION="LabResults.postman_collection.json"

    elif [ "${1,,}" == "labs" ] || [ "${1,,}" == "compendium" ] ; then
        COLLECTION="Labs.postman_collection.json"

    elif [ "${1,,}" == "notifications" ] ; then
        COLLECTION="Notifications.postman_collection.json"

    elif [ "${1,,}" == "orders" ] ; then
        COLLECTION="Orders.postman_collection.json"

    elif [ "${1,,}" == "orderstatus" ] ; then
        COLLECTION="OrderStatus.postman_collection.json"

    elif [ "${1,,}" == "partnerendpoint" ] ; then
        COLLECTION="PartnerEndpoint.postman_collection.json"

    elif [ "${1,,}" == "pendingorders" ] ; then
        COLLECTION="PendingOrders.postman_collection.json"

    elif [ "${1,,}" == "results" ] ; then
        COLLECTION="Results.postman_collection.json"

    elif [ "${1,,}" == "routes" ] ; then
        COLLECTION="Routes.postman_collection.json"

    elif [ "${1,,}" == "roundtrip" ] ; then
        COLLECTION="Round Trip (Order-Results).postman_collection.json"

    else
        echo "Collection (-c) not set, Running jwt token collection"
        COLLECTION="jwt_tokenGeneration.postman_collection.json"
    fi
}

setResults(){
    RESULTS=$1
}

updateJWT(){
    newman run "$DIR/../collection/jwt_tokenGeneration.postman_collection.json" -e "$DIR/../environment/$ENVIRONMENT.postman_environment.json" -g "$DIR/../globals/HoneyBadgers.postman_globals.json" --insecure -r cli,junit --reporter-cli-no-failures --reporter-cli-no-summary --reporter-junit-export $REPORT --export-environment "$DIR/../environment/$ENVIRONMENT.postman_environment.json"
}

runTests(){
    newman run "$DIR/../collection/$COLLECTION" -e "$DIR/../environment/$ENVIRONMENT.postman_environment.json" -g "$DIR/../globals/HoneyBadgers.postman_globals.json" --insecure -r cli,junit --reporter-cli-no-failures --reporter-cli-no-summary --reporter-junit-export $REPORT
}

runFunctionalTestSuite(){
    for key in "${!FUNCTIONALTESTSUITE[@]}"; do
        newman run "$DIR/../collection/${FUNCTIONALTESTSUITE[$key]}" -e "$DIR/../environment/$ENVIRONMENT.postman_environment.json" -g "$DIR/../globals/HoneyBadgers.postman_globals.json" --insecure -r cli,junit --reporter-cli-no-failures --reporter-cli-no-summary --reporter-junit-export "$DIR/../newman/$key.Report.xml"
    done
}

# -------------Results Parsing-------------
read_dom () {
    local IFS=\>
    read -d \< ENTITY CONTENT
    local ret=$?
    TAG_NAME=${ENTITY%% *}
    ATTRIBUTES=${ENTITY#* }
    return $ret
}

parse_dom () {
    if [[ $TAG_NAME = "system-err"  ]] ; then
        echo "FAIL: system-err"
        setResults "FAIL"
    elif [[ $TAG_NAME = "failure"  ]] ; then
        echo "failure"
        setResults "FAIL"
    elif [[ $TAG_NAME = "error"  ]] ; then
        echo "error"
        setResults "FAIL"
    fi
}

getResults(){
    REPORTFILES=$DIR/../newman/*.xml
    for f in $REPORTFILES;
    do
        while read_dom; do
            parse_dom
        done < $f
    done;
}
# -------------Results Parsing-------------


setEnvironment $E
setCollection $C

if [ "$COLLECTION" ==  "jwt_tokenGeneration.postman_collection.json" ] ; then
    runTests
elif [ "$COLLECTION" == "functional" ] ; then
    updateJWT
    runFunctionalTestSuite
else
    updateJWT
    runTests
fi

getResults
echo 
echo "------------------------------------------"
echo "Test Suite Results: $RESULTS" 
echo
echo 